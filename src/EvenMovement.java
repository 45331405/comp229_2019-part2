import java.util.*;
public class EvenMovement implements Movement{
    
    @Override
    public Cell CelltoMove(List<Cell> possibleLocs){
        Cell currLM = possibleLocs.get(0);
        for(Cell c: possibleLocs){
            if (c.leftOfComparison(currLM) < 0){
                currLM = c;
            }
        }
        return currLM;
    }

    @Override
    public String toString(){
        return "left-most movement strategy";
    }
}