import java.util.*;
public interface Movement{
   
    public Cell CelltoMove(List<Cell> possibleLocs);
}