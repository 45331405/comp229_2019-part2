import java.util.*;

public class GridIterator implements Iterator <Cell>{
    public Cell[][] list; // list of cells
    int i = 0; // - row
    int j = 0; // - column

    public GridIterator(Cell[][] cells) {
        this.list = cells; // when we call this function we make the list = to the cells of the grid.
    }

    // its a constat forloop
    @Override
    public boolean hasNext() {
        int tempI = i; // row
        int tempJ = j+1; // column + 1
        
        // j is bigger than the list lenght so the list is populated
        if(tempJ >= list[0].length){
            tempJ = 0; // we make them tempj = 0 
            tempI++;// we increae i
        }
        // row is bigger than the list and the colums are more than 1
        if (tempI < list.length && tempJ < list[0].length) {
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public Cell next() { 
        j++; // increse colum
        // list more than one
        if(j>= list[0].length) {
            j = 0;
            // plus one row
            i++;
        }
        // return the whole list
        return list[i][j];

    }
}