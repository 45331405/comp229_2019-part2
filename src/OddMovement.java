import java.util.*;
public class OddMovement implements Movement{
    @Override
    public Cell CelltoMove(List<Cell> possibleLocs){
        int i = (new Random()).nextInt(possibleLocs.size());
        return possibleLocs.get(i);
    }

    @Override
    public String toString(){
        return "random movement strategy";
    }
}